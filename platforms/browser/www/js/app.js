$('.collection')
    .on('click', '.collection-item', function(){
        var nomeProduto = this.firstChild.textContent;
        Materialize.toast(nomeProduto + ' adicionado', 1000);

        var $badge = $('.badge', this);
        if ($badge.length === 0) {
            $badge = $('<span class="badge brown-text">0</span>').appendTo(this);
        }

        $badge.text(parseInt($badge.text()) + 1);
    })
    .on('click', '.badge', function() {
        $(this).remove();
        return false;
    });


$('.modal-trigger').leanModal();


$('#confirmar').on('click', function() {
    var texto = "";

    $('.badge').parent().each(function(){
        texto += this.firstChild.textContent + ': ';
        texto += this.lastChild.textContent + ', ';
    });

    $('#resumo').empty().text(texto);
});


$('.acao-limpar').on('click', function() {
    
$('.scan-qrcode').on('click', function(){
cordova.plugin.barcodeScanner.scan( function (resultado) {
if (resultado.text) {
$('#numero-mesa').val(resutado.text);
} }, function (error) {
	Materialize.toast('Erro: ' + error, 3000, 'red-text');
   });
});

    $('.badge').remove();
});

